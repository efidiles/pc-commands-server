'use strict';

var _ = require('lodash');
var spawn = require('child_process').spawn;
var logger = require('../decorators/logger')('From main router');
var sleepMode = require('sleep-mode');
var address = require('network-address');

module.exports = {
  root: _root,
  address: _address,
  test: _test
};

function _root(req, res) {
  logger.debug('Inside root route');

  res.writeHeader(200, {
    "Content-Type": "text/html"
  });
  res.write('Great! The API is working.');

  return res.end();
}

function _address(req, res) {
  logger.debug('Inside address route');

  res.locals.success({
    address: address()
  });

  return res.end();
}

function _test(req, res) {
  logger.debug('Inside test route');

  sleepMode(function (err, stderr, stdout) {
    if (err) {
      logger.error(err);
    }
  });

  return res.end();
}
