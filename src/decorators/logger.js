'use strict';
/**
 * This package decorates a winston logger instance.
 * For example it decorates the debug function by adding prefixes to all debug messages.
 * The prefix is specified in the exports function.
 */

var winston = require('winston');
var _ = require('lodash');
var winstonInstance = _getWinstonInstance();

module.exports = _createDecorator;

function _createDecorator(modulePrefix) {
  return {
    debug: _invokeWithPrefix.bind(null, 'debug', modulePrefix),
    info: _invokeWithPrefix.bind(null, 'info', modulePrefix),
    error: _invokeWithPrefix.bind(null, 'error', modulePrefix)
  };
}

function _getWinstonInstance() {
  var consoleTransport = new(winston.transports.Console)({
    prettyPrint: true,
    json: false,
    colorize: true
  });

  var fileTransport = new(winston.transports.File)({
    filename: 'log-file.log',
    prettyPrint: true,
    json: false,
    colorize: true,
    maxsize: 10000000,
    handleExceptions: true,
    humanReadableUnhandledException: true
  });

  var winstonLogger = new(winston.Logger)({
    transports: [consoleTransport, fileTransport]
  });

  // Set logger debug level
  _.each(winstonLogger.transports, function (transport) {
    transport.level = 'debug';
  });

  return winstonLogger;
}

function _invokeWithPrefix(method, modulePrefix) {
  var args = (arguments.length === 1 ? [arguments[0]] : Array.apply(null, arguments));
  // Removes 'method and modulePrefix' params
  args = args.splice(2);
  // Appends the prefix of the module
  if (modulePrefix) {
    args[0] = modulePrefix + ' : ' + args[0];
  }
  winstonInstance[method].apply(winstonInstance, args);
}
