'use strict';

var logger = require('../decorators/logger')('From response helpers');

module.exports = responseHelpers;

function responseHelpers(req, res, next) {
  res.locals.error = _error.bind(null, res);
  res.locals.success = _success.bind(null, res);

  next();
}

function _error(res, statusCode, message, log) {
  if (log) {
    logger.debug(message);
  }

  var body = {
    message: message
  };

  res.status(statusCode).json(body);
}

function _success(res, data, log) {
  if (log) {
    logger.debug(data);
  }

  if (typeof data === 'string') {
    data = {
      message: data
    };
  }

  res.status(200).json(data);
}
