'use strict';

var express = require('express');
var compression = require('compression');
var bodyParser = require('body-parser');
var multer = require('multer');
var path = require('path');
var os = require('os');
var rimraf = require('rimraf');
var mkdirp = require('mkdirp');

var responseHelpers = require('./middlewares/response-helpers');
var routes = require('./routes');
var logger = require('./decorators/logger')('From index');

module.exports = appFactory;

function appFactory() {
  var app = express();

  loadMiddlewares(app);
  setupRoutes(app);

  logger.info('Started app in %s environment on port %s.',
    process.env.NODE_ENV,
    process.env.NODE_PORT
  );

  return app;
}

function loadMiddlewares(app) {
  app.use(compression());
  app.use(bodyParser.urlencoded({
    extended: true
  }));
  app.use(bodyParser.json());
  app.use(responseHelpers);
  app.use(setAccessHeaders);
  app.use(disableCaching);

  function setAccessHeaders(req, res, next) {
    res.header('Access-Control-Allow-Origin', 'http://localhost:3000');
    res.header('Access-Control-Allow-Methods', 'GET,POST,PUT,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type');

    if (req.method === 'OPTIONS') {
      res.status(200);
      return res.end();
    }

    next();
  }

  function disableCaching(req, res, next) {
    res.header('Cache-Control', 'private, no-cache, no-store, must-revalidate');
    res.header('Expires', '-1');
    res.header('Pragma', 'no-cache');

    next();
  }
}

function setupRoutes(app) {
  setupMainRouter();

  function setupMainRouter() {
    var mainRouter = express.Router();

    app.use('/', mainRouter);

    mainRouter.get('/', routes.main.root);
    mainRouter.get('/address', routes.main.address);
    mainRouter.get('/test', routes.main.test);
  }
}
